Elasticsearch:
The Elasticsearch servers are getting hit, hard, and they're not responding to all requests. Customers are getting "Bad Gateway" errors. How do we know how many shards to use? How to distribute them? How much per server? What capacity can we handle? When do we add more servers? And why the hell don't we have monitoring set up?!
