
////addd node
:::http://docs.hortonworks.com/HDPDocuments/HDP1/HDP-1.2.2/bk_reference/content/reference_chap6-2.html
wget -nv http://public-repo-1.hortonworks.com/HDP-1.2.0/repos/centos6/hdp.repo -O /etc/yum.repos.d/hdp.repo
///AS HADOOP >>> start node
/usr/lib/hadoop/bin/hadoop-daemon.sh --config /etc/hadoop/conf start datanode
///AS MAPRED >>>
/usr/lib/hadoop/bin/hadoop-daemon.sh --config
$HADOOP_CONF_DIR start tasktracker 
////refresh the mster namenode
su – hdfs –c “hadoop dfsadmin –refreshNodes”

////hadoop>>ssh-key

ssh-agent bash
ssh-add

SSH doesn’t like it if your home or ~/.ssh directories have group write permissions. Your home directory should be writable only by you, ~/.ssh should be 700, and authorized_keys should be 600 :

//SLAVE HOSTS FILE
#IPAddress     Hostname                  Alias
127.0.0.1    localhost
172.31.16.195 ec2-54-200-52-13.us-west-2.compute.amazonaws.com
//CORE-SITE VALUE (FQDN)
 <value>hdfs://ec2-54-69-249-181.us-west-2.compute.amazonaws.com:9000</value>
 //YARN-SITE VALUE (FQDN or IP)
 <value>54.69.249.181:8031</value>
<value>ec2-54-69-249-181.us-west-2.compute.amazonaws.com:8033</value>

//permissions in HUE
//edit hue/psuedo-distribution.ini
//add hue to hdfs & hadoop group
//chown hue:hdfs  hadoop/dfs

//java status
# jps


////API-CLI REFERENCE
http://hadoop.apache.org/docs/r1.0.4/webhdfs.html#ProxyUsers
http://www.cloudera.com/content/cloudera/en/documentation/cdh4/v4-3-0/CDH4-Installation-Guide/cdh4ig_topic_18_2.html

////SCANNING TWITTER DATA WITH FLUME
http://gethue.com/how-to-analyze-twitter-data-with-hue/



#HADOOOP
bin/hadoop namenode -format

#run hadoop
$ bash hadoop/sbin/start-all.sh  (starthadoop)
$ hadoop datanode
$ hive --service hiveserver
$ hue start/ hue runserver

#.............REST.................
curl -i "http://localhost:50070/webhdfs/v1/tmp?user.name=hadoop&op=GETFILESTATUS"
curl -i "http://localhost:50070/webhdfs/v1/huedata?user.name=hadoop&op=LISTSTATUS"
curl -i "http://localhost:50070/webhdfs/v1/testdata?user.name=hadoop&doas=hadoop&op=LISTSTATUS"

#.............HDFS................http://hadoop.apache.org/docs/r1.0.4/webhdfs.html....
hadoop fs -mkdir /home/hadoop/hadoop/testdata
hadoop fs -mkdir hdfs://localhost:9000/data
hadoop fs -mkdir hdfs://localhost:9000/name
hadoop fs -put <file>  hdfs://54.69.249.181:9000/testdata
hadoop fs -ls -d hdfs://localhost:9000/

#datanode style HDFS......
hadoop fs -ls hdfs://54.69.249.181:9000/testdata

/tmp/hadoop-hadoop/dfs/name


#FIND OPEN PORTS
sudo netstat -tulpn | grep :8020

#stop hadoop
hadoop/sbin/hadoop-daemon.sh --config $HADOOP_CONF_DIR stop

ec2-54-69-249-181.us-west-2.compute.amazonaws.com:50070

dfs -copyFromLocal /tmp/gutenberg /user/hduser/gutenberg
hduser@ubuntu:/usr/local/hadoop$ bin/hadoop dfs -ls /user/hduser

#hue runserver
/home/hadoop/hue/build/env/bin/supervisor -d
#development server
hue runcpserver 54.69.249.181:8000
#hue password
build/env/bin/hue  createsuperuser
build/env/bin/hue  changepassword

#///////////HUE SHELL//////////////
hue dbshell
.tables
hue runcpserver 54.69.249.181:8000


#Filesystem root '/' should be owned by 'hdfs ...
/home/hadoop/hue/desktop/libs/hadoop/src/hadoop/fs/webhdfs.py

hadoop dfsadmin -safemode leave
-refreshnodes
hadoop dfsadmin -report

hadoop fs -D dfs.replication=0 -copyFromLocal /workspace/datasets/Top100DiagnosisPayments.csv hdfs://localhost:9000/tmp

//repair ClusterID error
hdfs namenode -format -clusterId CID-31a9f88b-6ce3-4a3a-8ac4-c29f6b11836b

hs
///port 10000
hive --service hiveserver
hadoop fsck / -blocks -locations -files


#/////GENERATING & USING KEYS FOR NEW USER
#matt_personal in authorized keys only works
rm id_rsa*
sudo ssh-keygen -R hostname
ssh-agent bash
ssh-keygen -t rsa -P ""
ssh-keygen -p  ////change password
ssh-add
cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys
cat id_rsa.pub
nano authorized_keys
nano /etc/ssh/ssh_config
service ssh restart
service sshd restart
sudo cp hadoop.pem /home/ec2-user/
cp id_rsa matt_personal.pem
cp id_rsa.pub matt_personal.pem


slurm 
mtr
iftop
ranger
tig



#///////HIVE
~/hive/hcatalog/sbin/hcat_server.sh  start
hive> create schema mydata;  
hive> show databases;
database is accessible at localhost:50070

hive> set hive.metastore.warehouse.dir
CREATE EXTERNAL TABLE Payments(DRG_Definition STRING,Provider_Id STRING,Provider_Name STRING,Provider_Street_Address STRING,Provider_City STRING,Provider_State STRING,Provider_Zip_Code  STRING,Hospital_Referral_Region_Description  STRING,Total_Discharges  STRING,Average_Covered_Charges  STRING,Average_Total_Payments  STRING,Average_Medicare_Payments  STRING) location '/user/hive/warehouse/Payments';
LOAD  DATA  INPATH  '/workspace/datasets/Top100DiagnosisPayments.csv' OVERWRITE INTO TABLE Payments;


//thrift
hive --service metastore
start metastore before hiveserver


/JAR files for HIVE CLASSPATH	
/opt/hadoop/hadoop/share/hadoop/mapreduce


///////HBASE
bin/hbase shell


///////HUE
///start complete server
//make sure all permissions and ownership enabled in "hue" 
// may conflict with hadoop server.  Start hue first.
/home/hadoop/hue/build/env/bin/supervisor -d

service stop iptables
configure /etc/hosts:
master ec2-54-69-249-181.us-west-2.compute.amazonaws.com
hdfs://master:9000

./build/env/bin/hue runserver
and visit http://127.0.0.1:8000/ !

//show config opts
hue/build/env/bin/desktop config_help | less

//hadoop data:::

sudo -u hue bash



/////HCATALOG
HIVE_HOME/bin/hcat



    http://localhost:50070/ – web UI of the NameNode daemon
    http://localhost:50030/ – web UI of the JobTracker daemon
    http://localhost:50060/ – web UI of the TaskTracker daemon


MongoDB notes.py

#mongoimport --db test --collection restaurants --drop --file primer-dataset.json
#mongoexport --db test --collection traffic --out traffic.json

#show columns/fields
var message = db.accounts.findOne();
 for (var key in message) { print(key); }

#db.<collections>.createIndex( { comments: "text" }, { "$**": "text" } )
# $**  any field with text content: $**

db.test.count()
db.test.find()
show collections
show dbs
db.accounts.getIndexes()
db


#import CSV
mongoimport --db newaccounts --collection <name> --type csv --fields username,first,last,employeenumber,ad,city,st,zip

Start python notebook in background:
Ctrl+Z , type 'bg'  or 'fg'
"background" "foreground"

#fields
"account_number", 
"balance" , 
"firstname" , 
"lastname" ,
"age" , 
"gender" , 
"address" , 
"employer" , 
"email" , 
"city" , 
"state" 


///////////////////HDFS and MongoDB /////////////////
<MONGODB>
> var schematodo = db.tasks.findOne();
> for (var key in schematodo) { print (key) ; }


#hadoop dfsadmin -safemode leave
#refreshnodes
hadoop dfsadmin -report

hadoop fs -D dfs.replication=0 -copyFromLocal /workspace/datasets/Top100DiagnosisPayments.csv hdfs://localhost:9000/tmp

//repair ClusterID error
hdfs namenode -format -clusterId CID-31a9f88b-6ce3-4a3a-8ac4-c29f6b11836b
bin/hadoop namenode -format

#run hadoop
$ bash hadoop/sbin/start-all.sh  (starthadoop)
$ hadoop datanode
$ hive --service hiveserver
$ hue start/ hue runserver

#.............REST.................
curl -i "http://localhost:50070/webhdfs/v1/tmp?user.name=hadoop&op=GETFILESTATUS"
curl -i "http://localhost:50070/webhdfs/v1/huedata?user.name=hadoop&op=LISTSTATUS"
curl -i "http://localhost:50070/webhdfs/v1/testdata?user.name=hadoop&doas=hadoop&op=LISTSTATUS"

#.............HDFS................http://hadoop.apache.org/docs/r1.0.4/webhdfs.html....
hadoop fs -mkdir /home/hadoop/hadoop/testdata
hadoop fs -mkdir hdfs://localhost:9000/data
hadoop fs -mkdir hdfs://localhost:9000/name
hadoop fs -put <file>  hdfs://54.69.249.181:9000/testdata
hadoop fs -ls -d hdfs://localhost:9000/

#datanode style HDFS......
hadoop fs -ls hdfs://54.69.249.181:9000/testdata

/tmp/hadoop-hadoop/dfs/name

#addd node
:::http://docs.hortonworks.com/HDPDocuments/HDP1/HDP-1.2.2/bk_reference/content/reference_chap6-2.html
wget -nv http://public-repo-1.hortonworks.com/HDP-1.2.0/repos/centos6/hdp.repo -O /etc/yum.repos.d/hdp.repo
#AS HADOOP >>> start node
/usr/lib/hadoop/bin/hadoop-daemon.sh --config /etc/hadoop/conf start datanode
#AS MAPRED >>>
/usr/lib/hadoop/bin/hadoop-daemon.sh --config
$HADOOP_CONF_DIR start tasktracker
#refresh the mster namenode
su – hdfs –c “hadoop dfsadmin –refreshNodes”
